## System requirements
 - A Linux/Unix System
 - PHP

## Installation
 - Copy the contents of this project to a location on your server.
 - Edit directories and variables to your local settings:
	- Edit variables for process "albina_meteo_import" in tools/meteo/albina_meteo_import/data_interface/config.ini to your local settings
	- Edit variables for process "albina_meteo_create_json" in config/config.ini to your local settings
	- Edit variable PROJ_HOME in ./create_meteo_ogd.sh to your local settings
 - Run create_meteo_ogd.sh script to run process
 - Set local crontab to run create_meteo_ogd.sh script regularly