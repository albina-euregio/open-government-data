#!/bin/bash
echo "====================================================="
echo " Create ALBINA OGD meteo XML and meteo JSON   "
echo "====================================================="
date 2>&1 
# -------------------------------------------
# Create ALBINA OGD meteo XML and meteo JSON
# -------------------------------------------

PROJ_HOME=#### Your Directory. e.g. "/daten1/projects/" #### + "open-government-data/"
METEOVIEWER_HOME="/daten1/projects/meteoviewer/"

prog_import="tools/meteo/albina_meteo_import/transfer/import_external.php"
log_import="log/meteo/log-albina-import-ogd-meteo-xml.txt"
prog_create="tools/meteo/albina_meteo_create_json/albina_meteo_create_json.php"
log_create="log/meteo/log-albina-ogd-meteo-create-json.txt"

echo "===================================" >> $PROJ_HOME$log_import ; 
echo `date` >> $PROJ_HOME$log_import ; 
echo "===================================" >> $PROJ_HOME$log_import ; 
echo " --> $prog_import" 2>&1 
php -f $PROJ_HOME$prog_import >> $PROJ_HOME$log_import 2>&1 ;

echo "===================================" >> $PROJ_HOME$log_create ; 
echo `date` >> $PROJ_HOME$log_create ; 
echo "===================================" >> $PROJ_HOME$log_create ;
date 2>&1
echo " --> $prog_create" 2>&1
php -f $PROJ_HOME$prog_create $PROJ_HOME >> $PROJ_HOME$log_create 2>&1 ; 
	
date 2>&1 
