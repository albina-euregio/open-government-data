﻿
<?php


/** $data=array(
	"station_name" => "Branzoll",
    "lat" => 46.4065,
    "lon" => 11.3111,
    "elev" => 226,
    "parameter" => array
        (
            0 => array
                (
                    "code" => "TL",
                    "code_orig" => "LT",
                    "unit" => "°C",
                    "data" => array
                        (
                            0 => array
                                (
                                    "value" => 13.3,
                                    "time" => "2017-04-07T11:10:00CEST",
                                    "qFlag" => 1
                               ),

                            1 => array
                                (
                                    "value" => 13.1,
                                    "time" => "2017-04-07T11:00:00CEST",
                                    "qFlag" => 1
                               ),

                            2 => array
                                (
                                    "value" => 12.3,
                                    "time" => "2017-04-07T10:50:00CEST",
                                    "qFlag" => 1
                               )
                        )
                ),
			1 => array
                (
                    "code" => "SH",
                    "code_orig" => "SH",
                    "unit" => "cm",
                    "data" => array
                        (
                            0 => array
                                (
                                    "value" => 13.3,
                                    "time" => "2017-04-07T11:10:00CEST",
                                    "qFlag" => 1
                               ),

                            1 => array
                                (
                                    "value" => 13.1,
                                    "time" => "2017-04-07T11:00:00CEST",
                                    "qFlag" => 1
                               ),

                            2 => array
                                (
                                    "value" => 12.3,
                                    "time" => "2017-04-07T10:50:00CEST",
                                    "qFlag" => 1
                               )
                        )
                )
        )
);

*/

//-----------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------
class stationXML{
	public $data;
	
	function __construct($stationdata, $dest, $filename = false) { 
		if(!$filename){
			
			$filename = $stationdata['country'] . $stationdata['region']. $stationdata['subregion']. "_" .$stationdata['station_id'] .".xml";
		}
		
		
		$this->data = $stationdata;
		//$this->operator =  $operator;
		
		$this->get_xml($stationdata, $dest, $filename);
		
    }
		
	function get_measurements($parameter, $data){

		$measurement_string = "";

		foreach ($data as &$set) {

		$measurement_string .= '
						<app:stationMeasurement>
							<validTime meastype="measured">
								<TimeInstant gml:id="TimeID01">
									<timePosition>'.$set['time'].'</timePosition>						
								</TimeInstant>
							</validTime>
							<app:parameter>'.$parameter.'</app:parameter>		
							<app:value>'.$set['value'].'</app:value>
							<app:qFlag>'.$set['qFlag'].'</app:qFlag>			
						</app:stationMeasurement>
						';
		}				
		return $measurement_string;
					
	}

	//-----------------------------------------------------------------------------------------------------------------------

	function get_locationdata($locationdata){

		if(isset($locationdata['station_name'])){$station_name=$locationdata['station_name'];}else{$station_name="";}
		if(isset($locationdata['elev'])){$elev=$locationdata['elev'];}else{$elev="";}
		if(isset($locationdata['aspect'])){$aspect=$locationdata['aspect'];}else{$aspect="";}
		if(isset($locationdata['lat'])){$lat=$locationdata['lat'];}else{$lat="";}
		if(isset($locationdata['lon'])){$lon=$locationdata['lon'];}else{$lon="";}
		
		if(isset($locationdata['country'])){$country=intval($locationdata['country']);}else{$country="";}
		if(isset($locationdata['region'])){$region=intval($locationdata['region']);}else{$region="";}
		if(isset($locationdata['subregion'])){$subregion=intval($locationdata['subregion']);}else{$subregion="";}
		if(isset($locationdata['terrain'])){$terrain=$locationdata['terrain'];}else{$terrain="";}
		
		if($terrain==""){$terrain="0";}		

		
		$locationdata_string = "";

		$locationdata_string =	
		'<locRef>
			<ObsPoint>			
				<name>'.$station_name.'</name>
				<obsPointSubType>weather site</obsPointSubType>		
				
				<validElevation>
					<ElevationPosition uom="m">
						<position>'.$elev.'</position>
					</ElevationPosition>
				</validElevation>
				
				<validAspect>
					<AspectPosition>
						<position>'.$aspect.'</position>
					</AspectPosition>
				</validAspect>
				
				<pointLocation>			
					<gml:Point >
						<gml:pos>'.$lat.' '.$lon.'</gml:pos>
					</gml:Point>
				</pointLocation>
				
				<customData>		
					<app:LAWIS>					
						<app:location>
							<app:country>'.$country.'</app:country>
							<app:region>'.$region.'</app:region>
							<app:subregion>'.$subregion.'</app:subregion>
							<app:terrain>'.$terrain.'</app:terrain>
						</app:location>					
					</app:LAWIS>
				</customData>		
			</ObsPoint>
		</locRef>';	
		
		return $locationdata_string;
					
	}

	function get_metadata($metadata){

		$date = date('Y-m-d\\TH:i:sT',time());
		
		
		$data_param = $metadata['parameter'];
		$metadata_param = "";
		foreach ($data_param as &$set) {
				
			if(isset($set['code'])){$code=$set['code'];}else{$code="";}
			if(isset($set['unit'])){$unit=$set['unit'];}else{$unit="";}
			if(isset($set['interval'])){$interval=$set['interval'];}else{$interval="";}
			
			$metadata_param .=  '
							<app:parameter>
								<app:code>'.$code.'</app:code>
								<app:unit>'.$unit.'</app:unit>
								<app:interval>'.$interval.'</app:interval>
							</app:parameter>
							';

		}
		
		$metadata_string = "";
		
		
		
		if(isset($metadata['operator'])){$operator=$metadata['operator'];}else{$operator="";}
		if(isset($metadata['contact'])){$contact=$metadata['contact'];}else{$contact="";}
		if(isset($metadata['email'])){$email=$metadata['email'];}else{$email="";}
		if(isset($metadata['telNr'])){$telNr=$metadata['telNr'];}else{$telNr="";}
		if(isset($metadata['station_name'])){$station_name=$metadata['station_name'];}else{$station_name="";}
		if(isset($metadata['description'])){$description=$metadata['description'];}else{$description="";}
		if(isset($metadata['status'])){$status=$metadata['status'];}else{$status="1";}		
		if(isset($metadata['station_id'])){$station_id=$metadata['station_id'];}else{$station_id="";}
		if(isset($metadata['interval'])){$interval=$metadata['interval'];}else{$interval="";}
		if(isset($metadata['comments'])){$comments=$metadata['comments'];}else{$comments="";}
	
		
		
		$metadata_string =	
		'<metaDataProperty>
			<MetaData>			
				<dateTimeReport>'.$date.'</dateTimeReport>
				<language>DE</language>
				
				<srcRef>
					<Operation>				
						<name>'.$operator.'</name>					
						<contactPerson>
							<Person>
								<name>'.$contact.'</name>
							</Person>
						</contactPerson>					
						<customData>
							<app:LAWIS>							
								<app:email>'.$email.'</app:email>
								<app:telNr>'.$telNr.'</app:telNr>
							</app:LAWIS>
						</customData>						
					</Operation>				
				</srcRef>			
				
				<customData>
					<app:LAWIS>
						<app:globalname>'.$station_name.'</app:globalname>
						<app:description>'.$description.'</app:description>					
						<app:status>'.$status.'</app:status>										
						<app:stationNr>'.$station_id.'</app:stationNr>						
						<app:interval>'.$interval.'</app:interval>				
						<app:comments>'.$comments.'</app:comments>
						
						<!-- Meteorological parameter of station-->
						<app:meteoParameter>					
							'.$metadata_param.'											
						</app:meteoParameter>
						
					</app:LAWIS>
				</customData>			
			</MetaData>
		</metaDataProperty>';	
		
		return $metadata_string;
					
	}

//-----------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------
	
	function get_xml($data, $dest, $filename){

		//get location and metadata
		$stationLocationdata = $this->get_locationdata($data);
		$stationMetadata = $this->get_metadata($data);
		
		
		$data_param = $data['parameter'];
		$stationMeasurements = "";
		// get measurements for every parameter
		foreach ($data_param as &$set) {
					
			$stationMeasurements .= $this->get_measurements($set['code'], $set['data']);

		}		
	

// include location, metadata and measurements / finish XML string
$xml_string = <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<WeatherStation xmlns="http://caaml.org/Schemas/V5.0/Profiles/SnowProfileIACS" xmlns:gml="http://www.opengis.net/gml" xmlns:app="http://www.avalanches.org/snoprofiler/station" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://caaml.org/Schemas/V5.0/Profiles/SnowProfileIACS
http://caaml.org/Schemas/V5.0/Profiles/SnowprofileIACS/CAAMLv5_SnowProfileIACS.xsd" gml:id="SLF7245">
	<Site>
		$stationMetadata
		
		$stationLocationdata
	
		<customData>		
			<app:LAWIS>		
				$stationMeasurements
			</app:LAWIS>
		</customData>
	</Site>
</WeatherStation>
XML;

		$xml = new SimpleXMLElement($xml_string);	
		//write XML to dest-directory
		echo "Writing file $filename to $dest <br>\n";
		$xml->asXML($dest.$filename);
		
	
}
	
}

//echo "<br>TEST:<br><br>";








//$metadata = $station->getStationMeta();
//print_r($metadata);
//print_r($data);
?>
