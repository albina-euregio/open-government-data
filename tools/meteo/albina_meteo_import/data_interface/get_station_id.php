<?php
function get_station_id(){
	
	//*******************************//
	//			Configuration		 //
	//*******************************//	
	$config_file =  parse_ini_file("config.ini", true); 
	
	$output_dir = $config_file['config']['id_dest_dir'];
	$xml_output = $config_file['config']['xml_id_output'];
	$providers = $config_file['provider']['id'];
	$codes = $config_file['code']['co'];
	
	//******************************//
	
	$ogd_stations = array();
	
	$output = $output_dir . $xml_output;
	$string = '<?xml version="1.0" encoding="utf-8"?>
	';
	$string .= '<meteo_ogd>
		';

	foreach($providers as $provider){	
		switch($provider){  
		
			//*******************************//
			//			TIROL				 //
			//*******************************//		
			case "tirol":
				$src = $config_file[$provider.'_metadata']['src'];
				$code = $codes[$provider];
				$key = $config_file[$provider.'_metadata']['key'];
				$id_key = $config_file[$provider.'_map_attr']['station_id'];											
				
				$string .= '<provider country=' . '"AT" ' . 'region="' . $provider .'"'. ' code="'. $code .'"' . ' source="' . $src . '">
						';
				//Get JSON File from OGD Source
				$json = file_get_contents($src);
				$metadata = json_decode($json);	
				
				//Einlesen aller Stationen
				$features = $metadata->$key;

				//Foreach Stations - Get Station ID
				$string .='<stations>
						';

				foreach ( $features as $k) {
					$ogd_stations[] = $k->$id_key.';'. $provider . ';' . $code;
					$string .= '	<id region="' . $provider .'"'. ' code="'. $code .'">'.$k->$id_key.'</id>
						';
				}
				$string .= '</stations>
					</provider>
				';
				break;
			
			//*******************************//
			//			SÜDTIROL			 //
			//*******************************//				
			case "suedtirol":
				$src = $config_file[$provider.'_metadata']['src'];
				$code = $codes[$provider];
				$key = $config_file[$provider.'_metadata']['key'];
				$id_key = $config_file[$provider.'_map_attr']['station_id'];
								
				$string .= '<provider country=' . '"IT" ' . 'region="' . $provider .'"'. ' code="'. $code .'"' . ' source="' . $src . '">
						';

				//Get JSON File from OGD Source
				$json = file_get_contents($src);
				$metadata = json_decode($json);	

				//Einlesen aller Stationen
				$features = $metadata->$key;

				//Foreach Stations - Get Station ID
				$string .='<stations>
						';
						
				foreach ( $features as $k) {
					$ogd_stations[] = $k->properties->$id_key.";". $provider . ";" . $code;
					$string .= '	<id region="' . $provider .'"'. ' code="'. $code .'">'.$k->properties->$id_key.'</id>
						';
				}	
				$string .= '</stations>
					</provider>
				';
				break;
			
			//*******************************//
			//			TRENTINO	     	 //
			//*******************************//	
			case "trentino":
				$src = $config_file[$provider.'_metadata']['src'];
				$code = $codes[$provider];
				$key = $config_file[$provider.'_metadata']['key'];
				$id_key = $config_file[$provider.'_map_attr']['station_id'];
				
				$string .= '<provider  country=' . '"IT" ' . 'region="' . $provider .'"'. ' code="'. $code .'"' . ' source="' . $src . '">
						';

				//Get XML File from OGD Source
				$features = simplexml_load_file($src);
				//Foreach Stations - Get Station ID (Array and XML)
				$string .='<stations>
						';		
				foreach ( $features->children() as $station) {
					$ogd_stations[] = $station['code'].";". $provider . ";" . $code;
					$string .= '	<id region="' . $provider .'"' . ' code="'. $code .'">'.$station['code'].'</id>
						';
				}
				$string .= '</stations>
					</provider>
				';
				break;
		}
	}
	$string .= '</meteo_ogd>';
	
	#file_put_contents($output, $string);
	return $ogd_stations;

}
?>