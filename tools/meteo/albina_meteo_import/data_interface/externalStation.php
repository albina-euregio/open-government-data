﻿<?php
date_default_timezone_set('CET');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);


class externalStation{

    private $con;
	private $config;	

	private $operator;
	private $station_id;
	private $language;
	
	private $tbl_station;
	private $tbl_parameter;
	private $tbl_lang;

	private $map_location;
	
	private $format;
	
	private $config_key_metadata;
	private $metadata_src;
	private $metadata_key;
	private $metadata_src_param;
	
	private $station_name;
	private $lat;
	private $lon;
	private $elev;
	
	private $map_attr_station_id;
	private $map_attr_station_name;
	private $map_attr_lat;
	private $map_attr_lon;
	private $map_attr_elev;
	
	private $map_attr_param;
	private $map_attr_param_unit;
	private $map_attr_data_date;
	private $map_attr_data_value;	
	private $map_attr_param_unit_arr;
	
	private $map_param;
	private $map_units;
	
	private $config_key_data;
	private $data_src;	
	private $param_station_id;
	private $param_code;
	private $param_date_from;
	private $param_date_to;
	
	//Tirol spezifisch
	private $tirol_map_param_unit;
	
	
	
	
	
	
	function __construct($operator = false , $station_id = false, $map_location = false, $language = false) { 
		
		$this->station_id =  $station_id;
		$this->operator =  $operator;
		$this->language =  $language;
		$this->map_location = $map_location; 
		$this->connect();
		

		if(!$language){
			$language = "de";	
		}
		

    }

	private function connect(){
			$this->get_config();			
				
    /*
			$server =  $this->config['db']['server'];
			$user = $this->config['db']['user'];
			$pw = $this->config['db']['pw'];
			$database = $this->config['db']['database'];	
						
			$this->con = pg_connect("host=$server dbname=$database user=$user password=$pw") or die ("No connection to DB --> " . pg_last_error($this->con));
			pg_set_client_encoding($this->con, "UTF-8");	
				*/
	}

	
	private function get_config(){		
		$config_file =  parse_ini_file("config.ini", true); 
		$this->config = $config_file;    
		
		#$this->tbl_station =  $config_file['db']['tbl_station'];
		#$this->tbl_parameter =  $config_file['db']['tbl_parameter'];
		#$this->tbl_lang =  $config_file['db']['tbl_lang'];
		
		#$this->map_location =  $config_file['map_location'];
		
		
		$this->format = $config_file[$this->operator]['format'];
		
		$this->config_key_metadata =  $this->operator . "_metadata";
		$this->metadata_src = $config_file[$this->config_key_metadata]['src'];
		$this->metadata_key = $config_file[$this->config_key_metadata]['key'];
		$this->metadata_src_param = $config_file[$this->config_key_metadata]['src_param'];
		
		//Tirol spezifisch
		$this->tirol_map_param_unit = $config_file['tirol_map_param_unit'];
		
		$this->config_key_map_attr =  $this->operator . "_map_attr";
		$this->map_attr_station_id = $config_file[$this->config_key_map_attr]['station_id'];
		$this->map_attr_station_name = $config_file[$this->config_key_map_attr]['station_name'];
		$this->map_attr_lat = $config_file[$this->config_key_map_attr]['lat'];
		$this->map_attr_lon = $config_file[$this->config_key_map_attr]['lon'];
		$this->map_attr_elev = $config_file[$this->config_key_map_attr]['elev'];		
		
		$this->map_attr_param = $config_file[$this->config_key_map_attr]['param'];
		$this->map_attr_param_unit = $config_file[$this->config_key_map_attr]['param_unit'];
		$this->map_attr_data_date = $config_file[$this->config_key_map_attr]['data_date'];
		$this->map_attr_data_value = $config_file[$this->config_key_map_attr]['data_value'];

		$this->map_param =  $config_file[$this->operator . "_map_param"];
		$this->map_units =  $config_file["map_units"];
		$this->map_attr_param_unit_arr =  $config_file[$this->operator . "_map_attr_param_unit_arr"];
		
		
		$this->config_key_data =  $this->operator . "_data";
		$this->data_src = $config_file[$this->config_key_data]['src'];
	
		$this->param_station_id = $config_file[$this->config_key_data]['param_station_id'];
		$this->param_code = $config_file[$this->config_key_data]['param_code'];
		$this->param_date_from = $config_file[$this->config_key_data]['param_date_from'];
		$this->param_date_to = $config_file[$this->config_key_data]['param_date_to'];
		
	}
	
    /*
	private function disconnect(){      
		pg_close($this->con);	
        unset($this->con);
    }
	*/

	
	
	public function getStation(){
		$operator = $this->operator;
		
		$daten = array();
		
		switch ($operator) {
			case "suedtirol":
				//echo "operator: suedtirol";
				$daten = $this->getStationMeta();
					
				// add parameter measurements
				foreach ( $daten['parameter'] as $k => $v ) {
					//$act_param_data = $this->getParameterData($v['code_orig'], $v["code"], "201705070000", "201705082300");	
										
					$act_param_data = $this->getParameterData($v['code_orig'], $v["code"]);			
					$daten['parameter'][$k]['data'] = $act_param_data;
			
				}					
				
				break;
			case "trentino":
				//echo "operator: trentino";
				
				$daten = $this->getStationMeta_tre();
				$daten['parameter'] = $this->getStationParameter_tre();
				
				break;
			case "tirol":
				//echo "operator: tirol";
				$daten = $this->getStationMeta_tir();
				$daten['parameter'] = $this->getStationParameter_tir();
				#$daten['data'] = $this->getParameterData_tir();
				break;
		}

		return $daten;		
		
    }
	public function getStationMeta_tir() {
		$operator = $this->operator;
		
		$station_metadata = array();
		$map_location = $this->map_location;
		if($this->format == "JSON"){
			$json = file_get_contents($this->metadata_src);
			$metadata = json_decode($json);	
		}
		
		$features = $metadata->{$this->metadata_key};
				
			foreach ( $features as $k) {

				if ( $k->{$this->map_attr_station_id} == $this->station_id ) {
						
					$station_metadata['station_id'] = $this->station_id ;
					$station_metadata['operator'] = $this->operator;
					
											
					$locations = explode("-", $map_location);#[$this->station_id]);
								
					$station_metadata['country'] = $locations[0];
					$station_metadata['region'] = $locations[1];
					$station_metadata['subregion'] = $locations[2];
					
					$station_metadata['terrain'] = "";
					$station_metadata['station_name'] = $k->properties->name;
					$station_metadata['lat'] = $k->geometry->coordinates[1]; # {$this->map_attr_lat};
					$station_metadata['lon'] = $k->geometry->coordinates[0]; # {$this->map_attr_lon};
					$station_metadata['elev'] = $k->geometry->coordinates[2]; # {$this->map_attr_elev};
					$station_metadata['aspect'] = "";
					
					#$station_metadata['parameter'] = 
					$this->getStationParameter_tir();
				}
			}	
						
		return $station_metadata;
		
	}
	
	
	public function getStationMeta(){
		$operator = $this->operator;
       	$metadata = array();
		$station_metadata = array();
		$map_location = $this->map_location;
		if($this->format == "JSON"){
			
			$json = file_get_contents($this->metadata_src);
			$metadata = json_decode($json);	
		}		

		$features=$metadata->{$this->metadata_key};

		foreach ( $features as $k => $v ) {
						
			if ( $features[$k]->properties->{$this->map_attr_station_id} == $this->station_id ) {
											
				$station_metadata['station_id'] = $this->station_id;
				$station_metadata['operator'] = $this->operator;
							
				//!!!!!!!!!!!!
				$locations = explode("-", $map_location);#[$this->station_id]);
							
				$station_metadata['country'] = $locations[0];
				$station_metadata['region'] = $locations[1];
				$station_metadata['subregion'] = $locations[2];
				
				$station_metadata['terrain'] = "";
					
				$station_metadata['station_name'] = $features[$k]->properties->{$this->map_attr_station_name};
				$station_metadata['lat'] = $features[$k]->properties->{$this->map_attr_lat};
				$station_metadata['lon'] = $features[$k]->properties->{$this->map_attr_lon};
				$station_metadata['elev'] = $features[$k]->properties->{$this->map_attr_elev};
				$station_metadata['aspect'] = "";
						
				$station_metadata['parameter'] = $this->getStationParameter();
				}
			}
		return $station_metadata;				       	
    }

	public function getStationMeta_tre(){
       	$metadata = array();
		$station_metadata = array();
		$map_location = $this->map_location;
		
				
		if($this->format == "XML"){			
			$features = simplexml_load_file($this->metadata_src);	
		}		
		

		//HARDCODED!!!!!!!!!!!!!!!
		
		foreach ( $features->children() as $station) {			
			if ( (string)$station['code'] == $this->station_id ) {
		
				$station_metadata['station_id'] = $this->station_id;
				$station_metadata['operator'] = $this->operator;
				
				
				$locations = explode("-", $map_location);#[$this->station_id]);
				
				$station_metadata['country'] = $locations[0];
				$station_metadata['region'] = $locations[1];
				$station_metadata['subregion'] = $locations[2];
			
				$station_metadata['terrain'] = "";
				
				$station_metadata['station_name'] = (string)$station['name'];
				$station_metadata['lat'] = (string)$station['latitude'];
				$station_metadata['lon'] = (string)$station['longitude'];
				$station_metadata['elev'] = (string)$station['elevation'];
				$station_metadata['aspect'] = "";
			
			
				//$station_metadata['parameter'] = $this->getStationParameter_tre();
			}
		}	
		return $station_metadata;				       	
    }
	
	//***********************************************************//
	//			TRENTINO OLD SRC								//
	//*********************************************************//
	/**
	public function getStationMeta_tre(){
       	$metadata = array();
		$station_metadata = array();
		$map_location = $this->map_location;
		
				
		if($this->format == "XML"){			
			$features = simplexml_load_file($this->metadata_src);	
		}		
		

		
		foreach ( $features as $k => $v ) {
			
			if ( (string)$v->{$this->map_attr_station_id} == $this->station_id ) {
		
				$station_metadata['station_id'] = $this->station_id;
				$station_metadata['operator'] = $this->operator;
				
				
				$locations = explode("-", $map_location);#[$this->station_id]);
				
				$station_metadata['country'] = $locations[0];
				$station_metadata['region'] = $locations[1];
				$station_metadata['subregion'] = $locations[2];
			
				$station_metadata['terrain'] = "";
				
				$station_metadata['station_name'] = (string)$v->{$this->map_attr_station_name};
				$station_metadata['lat'] = (string)$v->{$this->map_attr_lat};
				$station_metadata['lon'] = (string)$v->{$this->map_attr_lon};
				$station_metadata['elev'] = (string)$v->{$this->map_attr_elev};
				$station_metadata['aspect'] = "";
			
			
				//$station_metadata['parameter'] = $this->getStationParameter_tre();
			}
		}	
		return $station_metadata;		
		       	
    }
	*/
	
	//***********************************************************//
	//				TODO: getStationParameter_tirol				//
	//*********************************************************//
	//Stationsparameter der jeweiligen Station auslesen und mit Einheit (config) in Array speichern
	public function getStationParameter_tir(){

		$map_param = $this->map_param;
		$tirol_units = $this->tirol_map_param_unit;
		$param_tirol = array("HS","HSD24","HSD48","HSD72","LT","LT_MAX","LT_MIN","RH","WG","WG_BOE","WR","N");
		$read_param = array();
		$values=array();
		$date ="";
		$station_parameter = array();
		
		//Array mit Paramter aus config
		$map_param_arr = $this->map_param;
		
		if($this->format == "JSON"){
			$json = file_get_contents($this->metadata_src);
			$metadata = json_decode($json);	
		}
		
		//Einlesen aller Stationen
		$features = $metadata->{$this->metadata_key};
		
		//Foreach Stations
		foreach ( $features as $k) {
			//Überprüfe Stations-ID
			if ( $k->{$this->map_attr_station_id} == $this->station_id ) {
				
				$param_array = (array) $k->properties;
				#print_r($param_array);
				if(array_key_exists('date',$param_array)){ 
					$date = $param_array["date"];
				} else {
					$date = "false";
				}
				
				foreach($param_array as $key => $value) {
					if(!in_array($key,$param_tirol)){
						unset($param_array[$key]);
					} else {
						#echo "*********OLD" .$key. " " . " " . $map_param[$key] . " " .$value . "\n";	
						if($key == "WG" || $key == "WG_BOE"){
							$value = $value / 3.6;
						}
						#echo "**********NEW" .$key. " " . " " . $map_param[$key] . " " .$value. "\n";	
						$read_param[] = $map_param[$key]; 
						$values[$map_param[$key]] = $value;
					}
				}			
			}
		}
	
		

		//if key von $map_param_array is in $read param, otherwise d
		foreach($read_param as $param){
			$act_param = array();
			$act_param["code"] = $param;
			$act_param["code_orig"] = $param;
			$act_param["unit"] = $tirol_units[$param];
				
			$dataset = array("value" => $values[$param], "time" => $date, "qFlag" => "1" );
			$act_param["data"] = array($dataset);
			array_push($station_parameter , $act_param);
		}
		return $station_parameter;	
	}
	
	public function getStationParameter(){
		
		$parameter = array();
		$station_parameter = array();

		$map_param_arr = $this->map_param;
		
		if($this->format == "JSON"){
		
			$json = file_get_contents($this->metadata_src_param);
			$parameter = json_decode($json);
			
			foreach ( $parameter as $k => $v ) {
					
				if ( $parameter[$k]->{$this->map_attr_station_id} == $this->station_id ) {
					$act_param = array();
					$act_param_code = $parameter[$k]->{$this->map_attr_param};
					
					$act_param["code"] =  $map_param_arr[$act_param_code];
					if($act_param["code"] == ""){$act_param["code"] =  $act_param_code;	}					
					
					$act_param["code_orig"] =  $act_param_code;
					
					//$act_param["name"] =  $parameter[$k]->{$this->map_attr_param_desc};
					$act_param["unit"] =  $parameter[$k]->{$this->map_attr_param_unit};
					
										
					//$act_param["data"] = $this->getParameterData($act_param_code, $act_param["code"]);
					
					array_push($station_parameter , $act_param);
					
				}
			}
			
			
		}
		return $station_parameter;		
		       	
    }
	
	//***********************************************************//
	//			TRENTINO NEW SRC								//
	//*********************************************************//	
	public function getStationParameter_tre(){
		
		$parameter = array();
		$station_parameter = array();
				
		$map_param_arr = $this->map_param;
		$map_attr_param_unit_arr = $this->map_attr_param_unit_arr;
		
		
		if($this->format == "XML"){
			$map_unit_arr = $this->map_units;
			$station_id = $this->station_id;
			$data_src = $this->data_src;	
			$param_station_id = $this->param_station_id;
		
			//print_r($map_unit_arr);
			#$url = "$data_src?$param_station_id=$station_id";
			#echo $url;
			$features = simplexml_load_file($data_src);

			foreach ( $features->children() as $station) {			
				if ( (string)$station['code'] == $this->station_id ) {
					
					foreach ( $map_param_arr as $code => $xml_path ) {
						$xml_path_parts = explode("/", $xml_path);
						$act_param = array();
						
						foreach ($station->children() as $parameterlist)
						{
		
							$parameter = $parameterlist->getName();
							
							if(strcmp($xml_path_parts[0],$parameter)==0){
								
								if($parameterlist->count()>0){
									##$path2 = $xml_path_parts[1];
									$act_param["code"] =  $code;
									$act_param["code_orig"] =  $xml_path;
									$act_param["data"] = array();
									
									foreach($parameterlist->children() as $param){
										$node = $param->getName();
										if(strcmp($xml_path_parts[1],$node)==0){
											$act_param["unit"] = (string)$param["UM"];
											$dataset = array();
											$dataset["value"] = (string)$param;
											$dataset["time"] = (string)$param["date"];
											$dataset["qFlag"] = "1";
											array_push($act_param["data"], $dataset);
										}
									}
								} 
							}						
		
						}
						if(!empty($act_param)){
							array_push($station_parameter , $act_param);
						}
					}
				}
			}
		}
			
		#print_r($station_parameter);
		return $station_parameter;		
		       	
    }

	
	/*
	public function getStationParameter_tre(){
		
		$parameter = array();
		$station_parameter = array();
				
		$map_param_arr = $this->map_param;
		$map_attr_param_unit_arr = $this->map_attr_param_unit_arr;
		
		
		if($this->format == "XML"){
			$map_unit_arr = $this->map_units;
			$station_id = $this->station_id;
			$data_src = $this->data_src;	
			$param_station_id = $this->param_station_id;
		
			//print_r($map_unit_arr);
			$url = "$data_src?$param_station_id=$station_id";
			#echo $url;
			$xml = simplexml_load_file($url);

				
			foreach ( $map_param_arr as $code => $xml_path ) {
			
				if($xml_path != ""){
					
					$xml_path_parts = explode("/", $xml_path);
						
					//echo "check parameter $code -> $xml_path: \n";				
							
					$act_param_xml = $xml->{$xml_path_parts[0]}[0]->{$xml_path_parts[1]};
				
					
					if($act_param_xml){	
					
					
						$act_param = array();						
						if(isset($map_attr_param_unit_arr[$code])){
							$map_attr_param_unit = $map_attr_param_unit_arr[$code];
						}else{
							$map_attr_param_unit = $map_attr_param_unit_arr["default"];
						}
						
						
						$act_param["code"] =  $code;
						$act_param["code_orig"] =  $xml_path;						
						
						//$act_param_unit =  (string)$act_param_xml[0][$this->map_attr_param_unit];
						$act_param_unit = (string)$act_param_xml[0][$map_attr_param_unit];
						
						if(isset($map_unit_arr[$act_param_unit])){
							$act_param["unit"] =  $map_unit_arr[$act_param_unit];
						}else{
							$act_param["unit"] = "";
						}
						
						if($act_param["unit"] == ""){$act_param["unit"] =  $act_param_unit;	}	

						
						
						
						
						$act_param["data"] = array();											
					
						foreach($act_param_xml as $result){							
							
							$dataset = array();
							//$dataset["parameter"] = $code;
							$dataset["value"] = round((float)($result->{$xml_path_parts[2]}),2);
							$dataset["time"] = (string)$result->{$this->map_attr_data_date};
							$dataset["qFlag"] = "1";
													
							array_push($act_param["data"], $dataset);		
							
						}
											
						array_push($station_parameter , $act_param);
												
					}
				}
				
			}
		
						
		}
		return $station_parameter;		
		       	
    }
	*/
	
	//***********************************************************//
	//				TODO: getParameterData_ tirol				//
	//*********************************************************//
	public function getParameterData_tir(){

	}
	
	
	public function getParameterData($code_orig, $code = false, $from = false, $to = false){
		
		$param_data = array();
		
		$station_id = $this->station_id;
		$data_src = $this->data_src;	
		$param_station_id = $this->param_station_id;
		$param_code = $this->param_code;
		$param_date_from = $this->param_date_from;
		$param_date_to = $this->param_date_to;
		
		$date_param = "";
		if($from && $to){
			$date_param = "&$param_date_from=$from&$param_date_to=$to";
		}
		
		$json_url = "$data_src?$param_station_id=$station_id&$param_code=$code_orig$date_param";
		$json = file_get_contents($json_url);
		$data = json_decode($json);
		foreach ( $data as $k => $v ) {				
					$dataset = array();
					//$dataset["parameter"] = $code;
					$dataset["value"] = round($data[$k]->{$this->map_attr_data_value}, 2);
					$dataset["time"] = $data[$k]->{$this->map_attr_data_date};
					$dataset["qFlag"] = "1";
					
					array_push($param_data, $dataset);					
		}
		return $param_data;		
	
	}
	
}

//echo "<br>"; 
//echo "END";

?>