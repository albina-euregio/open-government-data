<?php 
class Config {
    private static $instance = null;
    private $data;
    
    private function __construct() {
        // use config.ini
        $ini = __DIR__ . "/config.ini";
        $this->data = parse_ini_file($ini, true);
    }
    
    private function get_value($key) {
        if(isset($this->data[$key])) {
            return $this->data[$key];
        }
        return '';
    }
    
    public static function get_property($key) {
        if(self::$instance == null) {
            self::$instance = new Config(); 
        }
        return self::$instance->get_value($key);
    }
}
?>