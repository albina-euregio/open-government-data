﻿<?php

$curr_dir = dirname(__FILE__);
$config_file =  parse_ini_file($curr_dir."/../data_interface/config.ini", true); 

$www_dir=$config_file['config']['www_dir'];
$proj_dir=$www_dir.$config_file['config']['proj_dir'];
$proj_dir_transfer=$www_dir.$config_file['config']['proj_dir_transfer'];
$external_station_php=$proj_dir."/".$config_file['config']['external_station_php'];
$station_xml_php=$proj_dir."/".$config_file['config']['station_xml_php'];
$get_station_id_php=$proj_dir."/".$config_file['config']['get_station_id_php'];
$save_meteodata_xml_php = $proj_dir_transfer."/".$config_file['config']['save_meteodata_xml_php'];

include $external_station_php;
include $station_xml_php;
include $get_station_id_php;


#$import_stations =  $config_file['import_stations']['id'];
$dest_dir=$config_file['config']['dest_dir'];

#$map_location=$config_file['map_location'];
#$map_operator=$config_file['map_operator'];


//********************************************//
//			Get IDs from XML				  //
//********************************************//
/*$id_source = "station_id.xml";
$features = simplexml_load_file($id_source);
$ogd_stations = array();
foreach($features->children() as $provider){
	$region = $provider['region'];
	$code = $provider['code'];
	foreach($provider->children() as $stations){
		foreach($stations->children() as $station){
			$ogd_stations[] = $station . ";" . $region . ";" . $code;
		}
	}
}
*/

// Get Stations from: get_station_id.php through function: get_station_id()
$ogd_stations = get_station_id();
#var_dump($ogd_stations);

#print_r($ogd_stations);
// create xml files for external ogd stations   

foreach($ogd_stations as $ogd_station){

	$station_data = explode(";" , (string)$ogd_station);
	$station = $station_data[0];
	$operator = $station_data[1];
	$location = $station_data[2];
		echo $station;	
	
	
	// $location = $map_location[$station];
	// $operator = $map_operator[$location];
	// echo $location ." ".$operator. "<br>";	
	
	// get station data
	$station = new externalStation($operator, $station, $location, "de");
	$data = $station->getStation();
	
	//write XML
	$xml = new stationXML($data, $dest_dir);

 }

 // save/move other xml files for import 
//include $save_meteodata_xml_php;

?>