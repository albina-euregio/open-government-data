<?php
/*
 * #################################################
 * Fetch and Parse Meteo XML ALBINA Meteo Files
 * Write JSON Meteo File
 * Last edit: 26.03.2019 by Florian Korn
 * #################################################
 */
$proj_home = $argv[1];
header('Content-type: text/plain; charset=utf-8');
date_default_timezone_set('Europe/Vienna');

class xml_to_json
{
	//Define Variables
    private $xml_src_dir;
    private $json_dest_dir;
    private $filename;
    private $log_dest_dir;
    private $log_filename;
    private $filename_viewer;
    
    private $description;
    private $creator;
    private $copyright;
    private $date_format;
    
    private $xml_namespace;
    
    private $region;
    private $station;
    private $station_value;
    private $json_unit;
    private $json_param;
    private $json_value;
    private $xml_param;
    
    private $timebuffer;
    private $limit;
	
    private $err;
    // ===================================================
    // --- get config Data
    // ===================================================
    public function get_config($proj_home)
    {
        require_once ($proj_home . "config/config.inc");
        $date_fn = date("Y-m-d");
        $time_fn = date("His");
        
        $config_array = Config::get_property("albina_meteo_create_json");
        
        $this->xml_src_dir = $config_array['xml_src_dir'];
        $this->json_dest_dir = $config_array['json_dest_dir'];
        $this->filename = $date_fn . '_' . $time_fn . '_' . $config_array['filename'];
        $this->log_dest_dir = $config_array['log_dest_dir'];
        $this->log_filename = $date_fn . '_' . $time_fn . '_' . rtrim($config_array['filename'],'.json') . '_' . $config_array['log_filename'];
        $this->filename_viewer = $config_array['filename_viewer'];
        
        $this->description = $config_array['description'];
        $this->creator = $config_array['creator'];
        $this->copyright = $config_array['copyright'];
        $this->date_format = $config_array['date_format'];
        
        $this->xml_namespace = $config_array['xml_namespace'];
        
        $this->region = $config_array['id'];
        $this->xml_param = $config_array['xml_param'];
        
        $this->json_param = $config_array['json_param'];
        $this->json_unit = $config_array['json_unit'];
        $this->json_value = $config_array['json_value'];
        
        $this->station = $config_array['station'];
        $this->station_value = $config_array['station_value'];
        
        $this->timebuffer = $config_array['timebuffer'];
        $this->limit = $config_array['limit'];
    }
    
    public function get_station()
    {
        // ===================================================
        // --- Read XML Files
        // ===================================================
        $src_xml = $this->xml_src_dir;
        $scandir_output = scandir($src_xml);
        $meteo_xml_files = array();
        $errors = array();
        
        foreach ($scandir_output as $key => $value) {
            $input = $src_xml . "/" . $value;
            $ext = pathinfo($input, PATHINFO_EXTENSION);
            if (is_file($input) && $ext == "xml") {
                $meteo_xml_files[] = $value;
            }
        }
        if(empty($meteo_xml_files)){
            //$errors[] = " ********* No XML Files in Directory! ********* ";
            exit(" ***** NO XML FILES IN SOURCE DIRECTORY ***** ");
        }
        
        $description = $this->description;
        $creator = $this->creator;
        $copyright = $this->copyright;
        
        $json_param_array = $this->json_param;
        $json_unit_array = $this->json_unit;
        $xml_param_array = $this->xml_param;
        $value_time_array = array();
        
        
        $date = date("Y-m-d");
        $time = date("H:i:s");
        
        // ---------------------------------------------------
        // --- Build JSON Metadata String
        // ---------------------------------------------------
        $metadatastring = '{
  "definitions": {
  	"metadata": {
        "description": "' . $description . '",
        "creation_date": "' . $date . 'T' . $time . '",
        "creator": "' . $creator . '",
		"copyright": "' . $copyright . '"
    },
	"units": {';
        
        for ($i = 0; $i < sizeof($json_param_array); $i ++) {
            $metadatastring .= '"' . $json_param_array[$i] . '": "' . $json_unit_array[$json_param_array[$i]] . '",';
            if ($i + 1 == sizeof($json_param_array)) {
                $metadatastring = rtrim($metadatastring, ',');
            }
        }
        
        $metadatastring .= '}
  },
  "type": "FeatureCollection",
  "features": [';
        ######################################################
        // ---------------------------------------------------
        // --- Loop through XML FIles and retrieve Values
        // ---------------------------------------------------
		######################################################
        $count = 0;
        foreach ($meteo_xml_files as $file_x) {
            
            $count ++;
            $datenow = $date . ' ' . $time;
            $json_station_array = $this->station;
            $json_station_value_array = $this->station_value;
            $json_param_value_array = $this->json_value;
            $timePosition = array();
            $parameter = array();
            $value = array();
            $json_string_array = array();
            $timebuffer = $this->timebuffer;
            
            $xml = simplexml_load_file($src_xml . '/' . $file_x);
            
            // ---------------------------------------------------
            // --- Register XPATH Namespac
            // ---------------------------------------------------
            $xml_ns = $this->xml_namespace;
            
            $xml->registerXPathNamespace("a", $xml_ns["a"]);
            $xml->registerXPathNamespace("gml", $xml_ns["gml"]);
            $xml->registerXPathNamespace("app", $xml_ns["app"]);
            $xml->registerXPathNamespace("xsi", $xml_ns["xsi"]);
            
            // ---------------------------------------------------
            // --- Get Values from XML
            // ---------------------------------------------------
            $timePosition = $xml->xpath("a:Site/a:customData/app:LAWIS/app:stationMeasurement/a:validTime/a:TimeInstant/a:timePosition");
            $parameter = $xml->xpath("a:Site/a:customData/app:LAWIS/app:stationMeasurement/app:parameter");
            $value = $xml->xpath("a:Site/a:customData/app:LAWIS/app:stationMeasurement/app:value");
            $timePosition_edited = array();
            
            $values = array($timePosition, $parameter, $value, $timePosition_edited);
            
            $b = $xml->xpath("a:Site/a:metaDataProperty/a:MetaData/a:customData/app:LAWIS/app:globalname");
            $json_station_value_array['name'] = $b[0];
            
            $a = $xml->xpath("a:Site/a:metaDataProperty/a:MetaData/a:customData/app:LAWIS/app:stationNr");
            $json_station_value_array['id'] = $a[0];
            
            $c = $xml->xpath("a:Site/a:metaDataProperty/a:MetaData/a:srcRef/a:Operation/a:name");
            $json_station_value_array['region'] = $c[0];
            
            $region_array = $this->region;
            $json_station_value_array['country'] = $region_array[trim($json_station_value_array['region'])];
            
            $c = $xml->xpath("a:Site/a:locRef/a:ObsPoint/a:validElevation/a:ElevationPosition/a:position");
            $elevation = $c[0];
            
            $d = $xml->xpath("a:Site/a:locRef/a:ObsPoint/a:pointLocation/gml:Point/gml:pos");
            $coordinates = $d[0];
            $coordinates = explode(" ", $coordinates);
            $coord_lat = $coordinates[0];
            $coord_long = $coordinates[1];
            
            // Validate Coordinates and Elevation
            if(trim($coord_lat) == ""){$coord_lat="false";   $errors[] = $file_x . " - " . $coord_lat . " set to false";}
            if(trim($coord_long) == ""){$coord_long="false"; $errors[] = $file_x . " - " . $coord_long . " set to false";}
            if(trim($elevation) == ""){$elevation="false";  $errors[] = $file_x . " - " . $elevation . " set to false";}
			$elevation = round($elevation);
                
            // ---------------------------------------------------
            // --- check if DATA exists
            // ---------------------------------------------------       
            if (empty($timePosition)) {
                $json_station_value_array['date'] = "false";
                $errors[] = "File: " . $file_x . "  * * * NO DATA * * * \n";
            }elseif(empty(array_intersect($values[1], $xml_param_array))){
                $errors[] = "File: " . $file_x . "  * * * NO VALUES FOR SELECTED PARAMS * * * \n";
            }
            
            // Edit Date Array for Comparison
            for ($i = 0; $i < count($values[0]); $i ++) {
                $value_date_array = explode("T", $values[0][$i]);
                $value_datetime = $value_date_array[0] . " " . $value_date_array[1];
                $value_datetime = rtrim($value_datetime, 'CEST');
                $values[3][$i] = $value_datetime;
            }
            
            // ---------------------------------------------------
            // --- fill JSON_VALUE_ARRAY with data
            // ---------------------------------------------------
            
            //1. ÜBERPRÜFEN auf PARAMETER aus Config
            for($x = 0; $x < sizeof($values[0]); $x ++){
                $pa = $values[1][$x];
                if(!in_array($pa, $xml_param_array)){
                    $values[0][$x] ="false";
                    $values[1][$x] ="false";
                    $values[2][$x] ="false";
                    $values[3][$x] ="false";

                }
            }
            
            
            // ----------------------------------------------------------
            // --- Call function closestDate to retrieve current values
            // ----------------------------------------------------------
            if (! empty($values[0])) {
                $closestDate = $this->find_closestDate($values[3], $datenow);
                $json_station_value_array['date'] = $closestDate; // trim($values[0][$i]);;
                foreach ($xml_param_array as $xml_param) {
					#$json_param_value_array["wdir_not"] = "false";
                    $key = array_search($xml_param, $xml_param_array);
                    for ($i = 0; $i < sizeof($values[0]); $i ++) {
                        if (strcmp(trim((string) $values[1][$i]), $xml_param) == 0) {
                            $timediff = round((strtotime($closestDate) - strtotime($values[3][$i])) / 60);
                            if ($timediff < 0) {
                                abs($timediff);
                            }
                            if ($timediff <= $timebuffer) {
                                $json_param_value_array[$key] = $values[2][$i];
                                
                                if (strcmp($closestDate, $values[3][$i]) == 0) {
                                    $json_param_value_array[$key] = $values[2][$i];
									# ---------------------------------------------------
									# --- create wind direction notation for wdir values
									# ---------------------------------------------------
									if($key=="wdir"){						
										$a_wdir_notation = array("N"=>22.5,"NE"=>67.5,"E"=>112.5,"SE"=>157.5,"S"=>202.5,"SW"=>247.5,"W"=>292.5,"NW"=>337.5);
										foreach($a_wdir_notation as $nkey => $nvalue){
											if($json_param_value_array["wdir"] <= $nvalue){
												$wdir_notation = $nkey; 
												break;
											}
											$wdir_notation = "N";
										}
										$json_param_value_array["x_wdir"] = $wdir_notation;											
									}
                                    break;
                                    /*
                                     * if(empty(trim($values[0][$i])))
                                     * {
                                     * $json_station_value_array['date'] = "false";
                                     * $errors[] = "File: " . $file_x . " * * * date missing * * * \n";
                                     * } else {
                                     * $json_station_value_array['date'] = trim($values[0][$i]); // str_replace(" ", "T", $values[0][$i]);// . "CEST";
                                     * }
                                     */
                                    // $json_station_value_array['date'] = trim($values[0][$i]); // str_replace(" ", "T", $values[0][$i]);// . "CEST";
                                }
                            }
                        }
                    }
                    
                    // ---------------------------------------------------
                    // --- VALIDATION. Call Function validate
                    // ---------------------------------------------------
                    $validated_value = $this->validate($json_station_value_array['region'], $key, $json_param_value_array[$key]);
                    $json_param_value_array[$key] = $validated_value[0];
                    if (! empty($validated_value[1])) {
                        $errors[] = "File: " . $file_x . " - " . $validated_value[1] ."\n";
                    }
                }
            }
            # ------------------------------------------------------------------------------------------
            # --- format date and call function date_validation to check timediff (limit in config area)
            # ------------------------------------------------------------------------------------------
            $date_format = $this->date_format;
            if($json_station_value_array['date'] != "false"){
                $date_string = date_create($json_station_value_array['date']);
                $new_date_string = date_format($date_string, $date_format);
                $json_station_value_array['date'] = $new_date_string;
				$datenow = date('Y-m-d H:m');
				$datediff = $this->date_validation($datenow,$new_date_string,$this->limit);
				if($datediff){
					goto skip;
				}    
            }
            
            // ---------------------------------------------------
            // --- Build JSON Feature String
            // ---------------------------------------------------
            $string = '
  {
      "type": "Feature",
      "properties": {';
            $last_index = count($meteo_xml_files);
            
            for ($i = 0; $i < sizeof($json_station_array); $i ++) {
                if (strcmp($json_station_value_array[$json_station_array[$i]], "false") == 0) {
                    $string .= '"' . $json_station_array[$i] . '": ' . $json_station_value_array[$json_station_array[$i]] . ',';
                } else {
                    $string .= '"' . $json_station_array[$i] . '": "' . $json_station_value_array[$json_station_array[$i]] . '",';
                }
            }
            for ($i = 0; $i < sizeof($json_param_array); $i ++) {
				if($json_param_array[$i]=="x_wdir"){
					$string .= '"' . $json_param_array[$i] . '": "' . $json_param_value_array[$json_param_array[$i]] . '",';
				} else {
					$string .= '"' . $json_param_array[$i] . '": ' . $json_param_value_array[$json_param_array[$i]] . ',';
				}
                if ($i + 1 == sizeof($json_param_array)) {
                    $string = rtrim($string, ',');
                }
            }
            
            $string .= ' },
      "geometry": {"type": "Point","coordinates": [' . trim($coord_long) . ', ' . trim($coord_lat) . ', ' . trim($elevation) . ']}';
            $string .= "\n" . '  },';
            if ($last_index == $count) {
                $string = rtrim($string, ',');
            }
            
            $string_array[] = $string;
			
			############# ADDED BY FK 2018-11-28
			//GOTO after Datediff Validation
			skip:
        }
        
        // --- Connect JSON Features
        $jstring = "";
        foreach ($string_array as $string) {
            $jstring .= $string;
        }
        
        $footer = '
  ]
}';
        
        // ---------------------------------------------------
        // --- Build JSON Feature String #End
        // ---------------------------------------------------
        
        // --- Connect string parts and built final JSON String
        $json = $metadatastring . $jstring . $footer;
        
        // --- ++CALL FUNCTION++  save_file
        $this->save_file($json);
        
        // --- ++CALL FUNCTION++  write_log
        $this->write_log($meteo_xml_files, $errors);
    }
    
    
    // ===================================================
    // --- find closest from given date in array of dates
    // ===================================================
    public function find_closestDate($array, $date)
    {
        foreach ($array as $day) {
            $interval[] = abs(strtotime($date) - strtotime($day));
        }
        asort($interval);
        $closest = key($interval);
        return $array[$closest];
    }
	
	// ===================================================
    // --- validate Date of station, Calcs difference between datenow and Stationdate 
    // ===================================================
    public function date_validation($date_start, $date_end, $limit){
		$timeFirst  = strtotime($date_start);
		$timeSecond = strtotime($date_end);
		
		$differenceInSeconds = abs($timeSecond - $timeFirst);
		$differenceInMinutes = abs($differenceInSeconds/60);
		$differenceInHours = abs($differenceInMinutes/60);
		if($differenceInHours > $limit){
			return true;
		}
	}
	
    // ===================================================
    // --- validate value according to parameter
    // ===================================================
    public function validate($region, $param, $value)
    {
        $err = $this->err;
        // ---------------------------------------------------
        // --- check value only if exists
        // ---------------------------------------------------
        if ($value != "false") {
            // ---------------------------------------------------
            // --- check temperature
            // ---------------------------------------------------
            if ($param == "temp" or $param == "temp_max" or $param == "temp_min") {
				$value = floatval($value);
				$value = round($value,1);
                if ($value < - 50 or $value > 50) {
                    $value = "false";
                    $err = $param.' set to false.';
                }
				
            }
            // ---------------------------------------------------
            // --- check snow
            // ---------------------------------------------------
            if ($param == "snow" or $param == "snow24" or $param == "snow48" or $param == "snow72") {
                if ($value <= 0 or $value > 500) {
                    $value = "false";
                    $err = $param.' set to false.';
                } else {
                    $value = round($value);
                }
            }
            // ---------------------------------------------------
            // --- check rhum
            // ---------------------------------------------------
            if ($param == "rhum") {
                if ($value < 0 or $value > 100) {
                    $value = "false";
                    $err = $param.' set to false.';
                } else {
                    $value = round($value);
                }
            }
            // ---------------------------------------------------
            // --- check and convert wind m/s to km/h
            // ---------------------------------------------------
            if ($param == "wspd" or $param == "wgus") {
                if ($value < 0 or $value > 400) {
                    $value = "false";
                    $err = $param.' set to false.';
                } else {
                    #$value = floatval($value);
					$value = round($value);
					if($region != "trentino"){
						$value = round($value * 3.6);
					}
                }
            }
            if ($param == "wdir") {
                if ($value < 0 or $value > 360) {
                    $value = "false";
                    $err = $param.' set to false.';
                } else {
                    $value = round($value);
                }
            }
			if ($param == "wdir_not") {
				break;
            }
        }
        $return = array($value, $err);
        return $return;
    }
    
    // ---------------------------------------------------
    // --- Save File
    // ---------------------------------------------------
    public function save_file($content)
    {
        $dest_dir = $this->json_dest_dir;
        $fn = $this->filename;
        $output = $dest_dir . '/' . $fn;
        file_put_contents($output, $content);
        
        $fn_viewer = $this->filename_viewer;
        $output_viewer = $dest_dir . '/' . $fn_viewer;
        file_put_contents($output_viewer, $content);
    }
    
    // ---------------------------------------------------
    // --- Write Log - File
    // ---------------------------------------------------
    public function write_log($xmls, $errors)
    {
        $dest_dr = $this->log_dest_dir;
        $fname = $this->log_filename;
        $output = $dest_dr . '/' . $fname;
        
        $log_str = "====================================\n";
        $log_str .= "PARSE METEO XML -> WRITE JSON FILE \n";
        $log_str .= "Execution Date: " . date("Y-m-d") . "\n";
        $log_str .= "Execution Time: " . date("H:i:s") . "\n";
        $log_str .= "Source directory: " . $this->xml_src_dir . "\n";
        $log_str .= "Parsed XML Files: " . count($xmls) . " Files.\n";
        foreach ($xmls as $file_x) {
            $log_str .= $file_x . "\n";
        }
        $log_str .= "Destination Directory " . $this->json_dest_dir . "\n";
        $log_str .= "JSON File: " . $this->filename . "\n";
        $log_str .= "====================================\n";
        if (empty($errors)) {
            $log_str .= "No Errors!\n";
        } else {
            $log_str .= "ERROR Log: ". count($errors). " ERRORS \n";
            foreach ($errors as $error) {
                $log_str .= $error . "\n";
            }
        }
        $log_str .= "====================================\n";
        
        echo $log_str;
        file_put_contents($output, $log_str);
    }
}

$xml_json = new xml_to_json();
$xml_json->get_config($proj_home);
$xml_json->get_station();
?>